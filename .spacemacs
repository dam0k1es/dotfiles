;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
    This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(sml
     html
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press `SPC f e R' (Vim style) or
     ;; `M-m f e R' (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     (better-defaults :variables
                      better-defaults-move-to-beginning-of-code-first t
                      better-defaults-move-to-end-of-code-first t)
     (treemacs :variables
               treemacs-use-git-mode 'extended)
     (helm :variables
           helm-enable-auto-resize t)
     ibuffer
     eww
     (spacemacs-layouts :variables
                        persp-autokill-buffer-on-remove 'kill-weak
                        spacemacs-layouts-restrict-spc-tab t)
     speed-reading
     (git
      :variables git-enable-magit-todos-plugin t)
     (lsp :variables
          lsp-lens-enable t
          lsp-use-lsp-ui t
          lsp-ui-doc-enable t)
     openai
     (nixos :variables
            nixos-format-on-save t)
     (shell :variables
            shell-default-height 30
            shell-default-position 'bottom
            shell-scripts-format-on-save t)
     (python :variables
             python-backend 'lsp
             python-format-on-save t
             python-sort-imports-on-save t)
     (c-c++ :variables
            c-c++-backend 'lsp-clangd
            c-c++-lsp-enable-semantic-highlight t
            c-c++-enable-organize-includes-on-save t
            c-c++-enable-clang-format-on-save t)
     javascript
     go
     yaml
     vimscript
     (latex :variables
            latex-backemd 'lsp
            latex-enable-auto-fill t
            latex-enable-folding t
            latex-enable-magic t)
     csv
     emacs-lisp
     (org :variables
          org-enable-roam-support t
          org-enable-roam-ui t
          org-enable-github-support t
          org-enable-appear-support t)
     markdown
     (auto-completion :variables
                      auto-completion-enable-snippets-in-popup t
                      auto-completion-enable-sort-by-usage t
                      auto-completion-minimum-prefix-length 1
                      auto-completion-idle-delay 0.0
                      auto-completion-enable-help-tooltip t)
     (spell-checking :variables
                     enable-flyspell-auto-completion nil)
     (syntax-checking :variables
                      syntax-checking-auto-hide-tooltips 5)
     systemd
     )

   ;; List of additional packages that will be installed without being wrapped
   ;; in a layer (generally the packages are installed only and should still be
   ;; loaded using load/require/use-package in the user-config section below in
   ;; this file). If you need some configuration for these packages, then
   ;; consider creating a layer. You can also put the configuration in
   ;; `dotspacemacs/user-config'. To use a local version of a package, use the
   ;; `:location' property: '(your-package :location "~/path/to/your-package/")
   ;; Also include the dependencies as they will not be resolved automatically.
   dotspacemacs-additional-packages '(
                                        ;auctex-latexmk
                                      calfw
                                      calfw-org
                                      focus
                                      lsp-focus
                                      elpy                  ; Development via Tramp (SSH)
                                      py-autopep8           ; "
                                      org-fancy-priorities
                                      langtool
                                      org-caldav
                                      xclip
                                      ellama
                                      )

   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '()

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only)
  )

(defun dotspacemacs/init ()
  "Initialization:
    This function is called at the very beginning of Spacemacs startup,
    before layer configuration.
    It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need to
   ;; compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;;
   ;; WARNING: pdumper does not work with Native Compilation, so it's disabled
   ;; regardless of the following setting when native compilation is in effect.
   ;;
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; Name of executable file pointing to emacs 27+. This executable must be
   ;; in your PATH.
   ;; (default "emacs")
   dotspacemacs-emacs-pdumper-executable-file "emacs"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=$HOME/.emacs.d/.cache/dumps/spacemacs-27.1.pdmp
   ;; (default (format "spacemacs-%s.pdmp" emacs-version))
   dotspacemacs-emacs-dumper-dump-file (format "spacemacs-%s.pdmp" emacs-version)

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 3

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; Set `read-process-output-max' when startup finishes.
   ;; This defines how much data is read from a foreign process.
   ;; Setting this >= 1 MB should increase performance for lsp servers
   ;; in emacs 27.
   ;; (default (* 1024 1024))
   dotspacemacs-read-process-output-max (* 1024 1024)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. Spacelpa is currently in
   ;; experimental state please use only for testing purposes.
   ;; (default nil)
   dotspacemacs-use-spacelpa t

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default t)
   dotspacemacs-verify-spacelpa-archives t

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'hybrid

   ;; If non-nil show the version string in the Spacemacs buffer. It will
   ;; appear as (spacemacs version)@(emacs version)
   ;; (default t)
   dotspacemacs-startup-buffer-show-version t

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; b bhy your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
                                        ;dotspacemacs-startup-banner 'official
   dotspacemacs-startup-banner 'official

   ;; Scale factor controls the scaling (size) of the startup banner. Default
   ;; value is `auto' for scaling the logo automatically to fit all buffer
   ;; contents, to a maximum of the full image heimgeht and a minimum of 3 line
   ;; heights. If set to a number (int or float) it is used as a constant
   ;; scaling factor for the default logo size.
   dotspacemacs-startup-banner-scale 'auto

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `recents-by-project' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   ;; The exceptional case is `recents-by-project', where list-type must be a
   ;; pair of numbers, e.g. `(recents-by-project . (7 .  5))', where the first
   ;; number is the project limit and the second the limit on the recent files
   ;; within a project.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 3)
                                (todos . 5)
                                (agenda . 5))
                                        ;(bookmarks . 5)


   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Show numbers before the startup list lines. (default t)
   dotspacemacs-show-startup-list-numbers t

   ;; The minimum delay in seconds between number key presses. (default 0.4)
   dotspacemacs-startup-buffer-multi-digit-delay 0.4

   ;; If non-nil, show file icons for entries and headings on Spacemacs home buffer.
   ;; This has no effect in terminal or if "all-the-icons" package or the font
   ;; is not installed. (default nil)
   dotspacemacs-startup-buffer-show-icons t

   ;; Default major mode for a new empty buffer. Possible values are mode
   ;; names such as `text-mode'; and `nil' to use Fundamental mode.
   ;; (default `text-mode')
   dotspacemacs-new-empty-buffer-major-mode 'text-mode

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'emacs-lisp-mode

   ;; If non-nil, *scratch* buffer will be persistent. Things you write down in
   ;; *scratch* buffer will be saved and restored automatically.
   dotspacemacs-scratch-buffer-persistent t

   ;; If non-nil, `kill-buffer' on *scratch* buffer
   ;; will bury it instead of killing.
   dotspacemacs-scratch-buffer-unkillable nil

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list iss loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark spacemacs-light)

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spacelinex theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator arrow :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font or prioritized list of fonts. The `:size' can be specified as
   ;; a non-negative integer (pixel size), or a floating-point (point size).
   ;; Point size is recommended, because it's device independent. (default 10.0)
   dotspacemacs-default-font '("FiraCode Nerd Font"
                               :size 15.0
                               :weight normal
                               :width normal)

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m" for terminal mode, "<M-return>" for GUI mode).
   ;; Thus M-RET should work as leader key in both GUI and terminal modes.
   ;; C-M-m also should work in terminal mode, but not in GUI mode.
   dotspacemacs-major-mode-emacs-leader-key (if window-system "<M-return>" "C-M-m")

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts t

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current austrockne sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.1

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not Muse native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; If non-nil the frame is undecorated when Emacs starts up. Combine this
   ;; variable with `dotspacemacs-maximized-at-startup' in OSX to obtain
   ;; borderless fullscreen. (default nil)
   dotspacemacs-undecorated-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Show the scroll bar while scrolling. The auto hide time can be configured
   ;; by setting this variable to a number. (default t)
   dotspacemacs-scroll-bar-while-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t', `relative' or `visual' then line numbers are enabled in all
   ;; `prog-mode' and `text-mode' derivatives. If set to `relative', line
   ;; numbers are relative. If set to `visual', line numbers are also relative,
   ;; but only visual lines are counted. For example, folded lines will not be
   ;; counted and wrapped lines are counted as multiple lines.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :visual nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; When used in a plist, `visual' takes precedence over `relative'.
   ;; (default nil)
   dotspacemacs-line-numbers '(:visual t
                                       :disabled-for-modes dired-mode
                                       doc-view-mode
                                       pdf-view-mode
                                       :size-limit-kb 1000
                                       :realtive t)

   ;; Code folding method. Possible values are `evil', `origami' and `vimish'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil and `dotspacemacs-activate-smartparens-mode' is also non-nil,
   ;; `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil smartparens-mode will be enabled in programming modes.
   ;; (default t)
   dotspacemacs-activate-smartparens-mode nil

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc...
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; If nil then Spacemacs uses default `frame-title-format' to avoid
   ;; performance issues, instead of calculating the frame title by
   ;; `spacemacs/title-prepare' all the time.
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format rechtsnational for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Color highlight trailing whitespace in all prog-mode and text-mode derived
   ;; modes such as c++-mode, python-mode, emacs-lisp, html-mode, rst-mode etc.
   ;; (default t)
   dotspacemacs-show-trailing-whitespace t

   ;; Delete whitespace while saving Puffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'trailing

   ;; If non-nil activate `clean-aindent-mode' which tries to correct
   ;; virtual indentation of simple modes. This can interfere with mode specific
   ;; indent handling like has been reported for `go-mode'.
   ;; If it does deactivate it here.
   ;; (default t)
   dotspacemacs-use-clean-aindent-mode t

   ;; Accept SPC as y for prompts if non-nil. (default nil)
   dotspacemacs-use-SPC-as-y nil

   ;; If non-nil shift your number row to match the entered keyboard layout
   ;; (only in insert state). Currently supported keyboard layouts are:
   ;; `qwerty-us', `qwertz-de' and `querty-ca-fr'.
   ;; New layouts can be added in `spacemacs-editing' layer.
   ;; (default nil)
   dotspacemacs-swap-number-row nil

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil

   ;; If nil the home buffer shows the full path of agenda items
   ;; and todos. If non-nil only the file name is shown.
   dotspacemacs-home-shorten-agenda-source nil

   ;; If non-nil then byte-compile some of Spacemacs files.
   dotspacemacs-byte-compile nil

   ))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
    This function defines the environment variables for your Emacs session. By
    default it calls `spacemacs/load-spacemacs-env' which loads the environment
    variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
    See the header of this file for more information."
  (spacemacs/load-spacemacs-env)
  )

(defun dotspacemacs/user-init ()
  "Initialization for user code:
    This function is called immediately after `dotspacemacs/init', before layer
    configuration.
    It is mostly for variables that should be set before packages are loaded.
    If you are unsure, try setting them in `dotspacemacs/user-config' first."
  )


(defun dotspacemacs/user-load ()
  "Library to load while dumping.
    This function is called only while dumping Spacemacs configuration. You can
    `require' or `load' the libraries of your choice that will be included in the
    dump."
  )


(defun dotspacemacs/user-config ()
  "Configuration for user code:
    This function is called at the very end of Spacemacs startup, after layer
    configuration.
    Put your configuration code here, except for variables that should be set
    before packages are loaded."

    ;;;;;;;;;;;;;;;
  ;; USER DATA ;;
    ;;;;;;;;;;;;;;;

  ;; Set E-Mail
  (setq user-mail-address "Dam0k1es@member.fsf.org")

  ;; Authentication Files
  ;; (setq auth-sources '((:source "~/.emacs.d/private/.authinfo.gpg")))

  ;; Set Start of week
  (setq calendar-week-start-day 1)
  ;; Set Time Format
  (setq org-duration-format 'h:mm)

    ;;;;;;;;;;;;;;;;;;
  ;; ORG & AGENDA ;;
    ;;;;;;;;;;;;;;;;;;

  ;; Add Effort Estimates (https://orgmode.org/manual/Effort-Estimates.html) to Org-Agenda
  ;; See  https://emacs.stackexchange.com/questions/53272/show-effort-and-clock-time-in-agenda-view
  (setq org-agenda-prefix-format '((agenda . " %i %-12:c%?-12t%-6e% s")
                                   (todo . " %i %-12:c %-6e")
                                   (tags . " %i %-12:c")
                                   (search . " %i %-12:c")))

  ;; ~Getting Things Done from~ [[https://gettingthingsdone.com/][David Allen]]
  ;; ~implemented in Emacs by~ [[https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html][Nicolas Petton]]
  (with-eval-after-load
      'org
    (setq org-agenda-files '(
                             "~/Dokumente/.gtd/gtd.org" ; gtd - put all the objects
                             "~/Dokumente/.gtd/inbox.org" ; gtd - collect everything
                             "~/Dokumente/.gtd/tickler.org" ; gtd - put entries with a timestamp
                             "~/Dokumente/Studium/Master/Master.org" ; study - organize
                             "~/Dokumente/.gtd/someday.org" ; gtd - put entries without a timestamp
                                        ;                             "~/.emacs.d/private/org-caldav/Persnlich.org"
                                        ;                             "~/.emacs.d/private/org-caldav/Vorlesungen.org"
                                        ;                             "~/.emacs.d/private/org-caldav/Studientermine.org"
                                        ;                             "~/.emacs.d/private/org-caldav/Arbeitsblocker.org"
                                        ;                             "~/.emacs.d/private/org-caldav/Home.org"
                             ))


        ;;; [[https://github.com/coroa/org-caldav][org-caldav]]: Sync Agenda with Nextcloud
    (use-package org-caldav
      :defer t
      :init
      (setq org-caldav-url "https://murena.io/remote.php/dav/calendars/dam0k1es/")
      (setq org-caldav-calendars
            '((:calendar-id "agenda"
                            :files ("~/.emacs.d/private/org-caldav/Agenda.org")
                            :inbox  "~/Dokumente/.gtd/tickler.org")
              ;; (:calendar-id "arbeitsblockerics-importiert"
              ;;               :files ("~/Dokumente/Studium/Master/AkademischerMitarbeiter.org")
              ;;               :inbox  "~/.emacs.d/private/org-caldav/agenda.org")
              (:calendar-id "persnlich-4/"
                            :files ("~/.emacs.d/private/org-caldav/Persnlich.org")
                            :inbox  "~/.emacs.d/private/org-caldav/Persnlich.org")
              (:calendar-id "vorlesungen-2"
                            :files ("~/.emacs.d/private/org-caldav/Vorlesungen.org")
                            :inbox  "~/.emacs.d/private/org-caldav/Vorlesungen.org"
                            )
              (:calendar-id "studientermine-1"
                            :files ("~/.emacs.d/private/org-caldav/Studientermine.org")
                            :inbox  "~/.emacs.d/private/org-caldav/Studientermine.org")
              (:calendar-id "home"
                            :files ("~/.emacs.d/private/org-caldav/Home.org")
                            :inbox  "~/.emacs.d/private/org-caldav/Home.org")))
      :config
      (setq org-icalendar-include-todo t)
      (setq org-icalendar-use-deadline '(event-if-todo event-if-not-todo todo-due))
      (setq org-icalendar-use-scheduled '(todo-start event-if-todo event-if-not-todo))
      (setq org-caldav-delete-calendar-entries 'never))

    ;; Syncronize when saving tickler.org
    (defun auto-sync ()
      (when (and (stringp buffer-file-name)
                 (string-match "tickler.org" buffer-file-name))
        (org-caldav-sync)))
    (add-hook 'after-save-hook 'auto-sync)


    ;; [[https://github.com/kiwanami/emacs-calfw][emacs-calfw]]: Add Calendar Interface
                                        ;(use-package calfw)
                                        ;(use-package calfw-org
                                        ;  :config
                                        ;  (setq cfw:org-overwrite-default-keybinding t
                                        ;        calendar-week-start-day 1)
                                        ;  (setq cfw:fchar-junction ?╬
                                        ;        cfw:fchar-vertical-line ?║
                                        ;        cfw:fchar-horizontal-line ?═
                                        ;        cfw:fchar-left-junction ?╠
                                        ;        cfw:fchar-right-junction ?╣
                                        ;        cfw:fchar-top-junction ?╦
                                        ;        cfw:fchar-top-left-corner ?╔
                                        ;        cfw:fchar-top-right-corner ?╗))

    ;; Do not list some files on recent files
    (setq recentf-exclude (org-agenda-files))
    (setq recentf-exclude '("/org-caldav/"
                            "/.gtd/"
                            "~/.emacs.d*/"
                            "/tmp/"
                            "Learn.org"))

    ;; Set a default target file for notes
    (setq org-capture-templates '(("t" "Todo [inbox]" entry
                                   (file+headline "~/Dokumente/.gtd/inbox.org" "Tasks")
                                   "* TODO %i%?")
                                  ("s" "Scheduled [tickler]" entry
                                   (file+headline "~/Dokumente/.gtd/tickler.org" "Tickler")
                                   "* %i%? \n %U")
                                  ("S" "Someday [someday]" entry
                                   (file+headline "~/Dokumente/.gtd/someday.org" "Someday")
                                   "* HOLD %i%?")))
    (setq org-refile-targets (quote (("gtd.org" :maxlevel . 1)
                                     ("tickler.org" :level . 1)
                                     ("processed.org" :level . 1)
                                     ("someday.org" :level . 1))))

    ;; Do not dim blocked tasks
    (setq org-agenda-dim-blocked-tasks nil)

    ;; Stop preparing agenda buffers on startup
    (setq org-agenda-inhibit-startup t)

    ;; Disable tag inheritance for agendas
    (setq org-agenda-use-tag-inheritance nil)

    ;; Define Keywords for TODO elements
    (setq org-todo-keywords
          '((sequence "TODO" "IN-PROGRESS" "HOLD" "FEEDBACK" "|" "SUCCESS" "FAILED" "CANCELED")))
    (setq org-todo-keyword-faces
          '(("TODO" :foreground "blue" :weight bold)
            ("IN-PROGRESS" :foreground "yellow" :weight bold)
            ("HOLD" :foreground "gray" :weight italic)
            ("FEEDBACK" :foreground "purple" :weight italic)
            ("SUCCESS" :foreground "green")
            ("FAILED" :foreground "red")
            ("CANCELED" :foreground "orange" :weight italic)))

    ;; Disable completion in src blocks
    (setq org-src-tab-acts-natively nil)

    ;; Set export backends
    (setq org-export-backends '(ascii beamer html icalendar latex man md odt))

    ;; [[https://github.com/org-roam/org-roam][Org-roam]]: Org-roam is a plain-text knowledge management system
    (setq org-roam-directory "~/Dokumente/Knowledge")
    (org-roam-db-autosync-mode)
    )
      ;;;;;;;;;;;;
  ;; VISUAL ;;
      ;;;;;;;;;;;;

  (with-eval-after-load
      'org
    ;; Display inline Images on opening org buffers
    (setq org-startup-with-inline-images t
          org-image-actual-width '(300))

    ;; Hide Markup Elements
    (setq org-hide-emphasis-markers t)

    ;; Toggle display of entities as UTF-8 characters
    (setq org-pretty-entities t)

    ;; Show Task in Spaceline
    (spaceline-toggle-org-clock-on)

    ;; Enable emphasis markers in insert mode
    (setq org-appear-trigger 'manual)
    )

  ;; Break lines when appropriate
  (add-hook 'text-mode-hook 'auto-fill-mode)

  ;; Show Time
  (display-time-mode 1)

  ;; Insert Unicode Symbols
  (global-prettify-symbols-mode 1)

  ;; Dim surrounding text
  (focus-mode 1)

  ;; Only show current task time
  (setq org-clock-mode-line-total 'current)

  ;; Enable transparency
  (add-hook 'after-make-frame-functions 'spacemacs/enable-transparency)

      ;;;;;;;;;;;;;;;;;;;;;
  ;; FILES & HISTORY ;;
      ;;;;;;;;;;;;;;;;;;;;;

  ;; Disable lock files
  (setq create-lockfiles nil)

  ;; Reload the file if the fiele changed on the filesystem
  (global-auto-revert-mode 1)

  ;; Do not backup decrypted Data
  (defun unleak-gpg ()
    (when (and buffer-file-name (string-match epa-file-name-regexp buffer-file-name))
      (message "Backup inhibited for this file")
      (setq-local backup-inhibited t)
      (auto-save-mode -1)))
  (add-hook 'find-file-hook #'unleak-gpg)

  ;; Enable autosaving recentf
  (recentf-mode 1)
  (setq recentf-max-menu-items 50)
  (setq recentf-max-saved-items 50)
  (run-at-time nil (* 5 60) 'recentf-save-list)

      ;;;;;;;;;;;;;;;;;
  ;; DEVELOPMENT ;;
      ;;;;;;;;;;;;;;;;;

  ;; (setq encrypted-content
  ;;       (with-temp-buffer
  ;;         (insert-file-contents "~/.openai.txt.gpg")
  ;;         (buffer-substring-no-properties (point-min) (point-max))))

  ;; (setq openai-key
  ;;       (replace-regexp-in-string "\n" ""
  ;;                                 (shell-command-to-string (concat "gpg --quiet -d " "~/.openai.txt.gpg"))))
  ;; (setq openai-user "user")

  ;; (openai-completion :model "text-davinci-003")

  ;; Create Templates for Programming Languages
  (auto-insert-mode 1)
  (setq auto-insert-directory "~/Vorlagen/prog")
  (define-auto-insert "\.c$" "c.c")
  (define-auto-insert "\.cpp$" "c++.cpp")
  (define-auto-insert "\.cs$" "c#.cs")
  (define-auto-insert "\.h$" "header.h")
  (define-auto-insert "\.html$" "html.html")
  (define-auto-insert "\.java$" "java.java")
  (define-auto-insert "\.py$" "python3.py")
  (define-auto-insert "\.sh$" "bash-sh.sh")
  (define-auto-insert "\.tex$" "LaTeX.tex")

  ;; Enable global Snippets
  (yas-global-mode 1)

  ;; Enable global Syntax Checking
  (global-flycheck-mode 1)

  ;; Enable global autocompletion
  (global-company-mode 1)

  (defun developer-mode ()
    (interactive)
    (lsp-mode)
    (lsp-ui-mode)
    (lsp-lens-mode)
    (lsp-focus-mode)
    )

  ;; Enable overwriting selected text
  (delete-selection-mode 1)

  ;; Set default terminal for terminal-here command
  (setq terminal-here-terminal-command 'tilix)

  ;; [[https://github.com/s-kostyaev/ellama?tab=readme-ov-file#ellama-context-add-selection][Ellama]] ~Enable Ollama~
  (use-package ellama
    :init
    (require 'llm-ollama)
    (setopt ellama-keymap-prefix "C-c e")
    (setopt ellama-language "German")
    (setopt ellama-provider
            (make-llm-ollama
             :chat-model "magicoder:latest"
             :embedding-model "magicoder:latest"))
    )

      ;;;;;;;;;;;;
  ;; OFFICE ;;
      ;;;;;;;;;;;;

  ;; [[https://github.com/mhayashi1120/Emacs-langtool][Emacs-langtool]]: ~Enable LanguageTool~
  (use-package langtool
    :defer t)
  ;; (setq langtool-default-language "de-DE")
  ;; (setq langtool-mother-tongue "de")
  (setq langtool-http-server-host "localhost"
        langtool-http-server-port 8081)

  ;; Enable Multi Language Support
  (with-eval-after-load "ispell"
    (setq ispell-program-name "hunspell")
    (ispell-set-spellchecker-params)
    (ispell-hunspell-add-multi-dic "de_DE,en_GB")
    (setq ispell-dictionary "de_DE,en_GB"))

  ;; Keep me off Google. Better to use Brave
  (setq helm-google-suggest-search-url "https://search.brave.com/search?q=")

  ;; Enable LaTeX Presentations
  (use-package auctex-latexmk
    :defer t
    :config (auctex-latexmk-setup)
    (setq auctex-latexmk-inherit-TeX-PDF-mode t))
  (require 'ox-beamer)
  (use-package ox-latex
    :defer t)

  (with-eval-after-load
      'org

    ;; Enable colored Org-Export LaTeX Listings
    (setq org-latex-listings 'minted
          org-latex-packages-alist '(("" "minted"))
          org-latex-pdf-process
          '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
            "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
    (setq org-latex-minted-options '(("breaklines" "true")
                                     ("breakanywhere" "true")
                                     ("breaksymbolleft=")
                                     ("breaksymbolright=")
                                     ("breaksymbolsepleft=")
                                     ("breaksymbolsepright=")
                                     ("breaksymbolindentleft=")
                                     ("breaksymbolindentright=")
                                     ("breakanywheresymbolpre=")
                                     ("breakanywheresymbolpost=")
                                        ;("linenos")
                                     ("xleftmargin=20pt")
                                     ("frame=lines")))

    ;; Disable Hyperref Flags
    (customize-set-value 'org-latex-with-hyperref nil)

    )

      ;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; SHORTCUT DEFINITIONS ;;
      ;;;;;;;;;;;;;;;;;;;;;;;;;;

  (global-set-key (kbd "M-y") 'helm-show-kill-ring)

  (with-eval-after-load
      'org

    (global-set-key (kbd "C-c l") #'org-store-link)
    (global-set-key (kbd "C-c a") #'org-agenda)
    (global-set-key (kbd "C-c c") #'org-capture)

    (spacemacs/set-leader-keys "aorra" 'org-roam-ref-add)
    (spacemacs/set-leader-keys "aorrr" 'org-roam-ref-remove)
    (spacemacs/set-leader-keys "aorrf" 'org-roam-ref-find)
    (spacemacs/set-leader-keys "aorre" 'org-roam-ref-read)
    )

  (spacemacs/declare-prefix "." "Ollama")
  (spacemacs/set-leader-keys ". ." 'ellama-provider-select)
  (spacemacs/set-leader-keys ". l" 'ellama-chat)
  (spacemacs/set-leader-keys ". a" 'ellama-ask-selection)
  (spacemacs/set-leader-keys ". C" 'ellama-complete)
  (spacemacs/set-leader-keys ". t" 'ellama-translate)
  (spacemacs/set-leader-keys ". d" 'ellama-define-word)
  (spacemacs/declare-prefix ". s" "Summarize")
  (spacemacs/set-leader-keys ". ss" 'ellama-summarize)
  (spacemacs/set-leader-keys ". sw" 'ellama-summarize-webpage)
  (spacemacs/declare-prefix ". c" "Code")
  (spacemacs/set-leader-keys ". ca" 'ellama-code-add)
  (spacemacs/set-leader-keys ". cc" 'ellama-code-complete)
  (spacemacs/set-leader-keys ". cr" 'ellama-code-review)
  (spacemacs/set-leader-keys ". ci" 'ellama-code-improve)
  (spacemacs/declare-prefix ". m" "Make")
  (spacemacs/set-leader-keys ". ml" 'ellama-make-list)
  (spacemacs/set-leader-keys ". mt" 'ellama-make-table)
  (spacemacs/set-leader-keys ". mf" 'ellama-make-format)
  (spacemacs/declare-prefix ". i" "Improve")
  (spacemacs/set-leader-keys ". iw" 'ellama-improve-wording)
  (spacemacs/set-leader-keys ". ig" 'ellama-improve-grammer)
  (spacemacs/set-leader-keys ". ic" 'ellama-improve-conciseness)

  (spacemacs/set-leader-keys "S C-b" 'langtool-check)
  (spacemacs/set-leader-keys "S S-b" 'langtool-check-done)
  (spacemacs/set-leader-keys "S C-d" 'langtool-switch-default-language)
  (spacemacs/set-leader-keys "S C-n" 'langtool-goto-next-error)
  (spacemacs/set-leader-keys "S C-p" 'langtool-goto-previous-error)
  (spacemacs/set-leader-keys "S C-s" 'langtool-correct-at-point)
  (spacemacs/set-leader-keys "S S-s" 'langtool-interactive-correction)

  (spacemacs/set-leader-keys "o f" 'focus-mode)
  )


;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
  (custom-set-variables
   ;; custom-set-variables was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(TeX-command-extra-options "-shell-escape")
   '(custom-safe-themes
     '("bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" "fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" default))
   '(evil-want-Y-yank-to-eol nil)
   '(helm-google-suggest-search-url "https://search.brave.com/search?q=")
   '(ignored-local-variable-values '((eval progn (pp-buffer) (indent-buffer))))
   '(ispell-dictionary nil)
   '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
   '(org-agenda-files
     '("/home/swinker/Dokumente/.gtd/gtd.org" "/home/swinker/Dokumente/.gtd/inbox.org" "/home/swinker/Dokumente/.gtd/tickler.org" "/home/swinker/Dokumente/Studium/Master/Master.org" "/home/swinker/Dokumente/.gtd/someday.org"))
   '(org-caldav-todo-percent-states
     '((0 "TODO")
       (50 "ONGOING")
       (0 "HOLD")
       (100 "SUCCESS")
       (100 "FAILED")))
   '(org-duration-format 'h:mm)
   '(org-latex-tables-centered t)
   '(package-selected-packages
     '(ellama chatgpt codegpt dall-e reveal-in-folder openai tblui tablist magit-popup py-autopep8 elpy ob-sml sml-mode company-nixos-options helm-comint helm-nixos-options json-mode json-navigator hierarchy json-reformat json-snatcher nixos-options eaf sqlite3 texfrag simpleclip config-general-mode nix-mode treepy async floobits highlight spray org-kanban yaml-mode company-quickhelp persistent-scratch magit-todos f compat nyan-mode lsp-focus focus evil-commentary org-caldav groovy-imports pcache groovy-mode lsp-java maven-test-mode mvn centaur-tabs closql ess-R-data-view ess pdf-tools ccls helm-lsp lsp-latex lsp-origami origami lsp-ui helm-bibtex magic-latex-buffer org-ref ox-pandoc citeproc bibtex-completion biblio biblio-core parsebib nav-flash org-appear ox-gfm helpful elisp-refs flyspell-popup treemacs posframe typo ibuffer-projectile dactyl-mode vimrc-mode packed evil-tex helm git-modes helm-core projectile orgit-forge orgit forge ghub emacsql-sqlite emacsql magit magit-section git-commit with-editor evil paredit smartparens systemd journalctl-mode xclip import-js grizzl js-doc js2-refactor multiple-cursors livid-mode nodejs-repl npm-mode skewer-mode js2-mode tern doom-modeline shrink-path add-node-modules-path company-web web-completion-data counsel-css emmet-mode helm-css-scss impatient-mode simple-httpd prettier-js pug-mode sass-mode haml-mode scss-mode slim-mode tagedit web-beautify web-mode bmx-mode powershell auctex-latexmk wolfram langtool org-fancy-priorities blacken code-cells company-anaconda anaconda-mode company counsel-gtags counsel swiper ivy cython-mode dap-mode lsp-docker lsp-treemacs bui yaml ggtags helm-cscope helm-gtags helm-pydoc importmagic epc ctable concurrent deferred live-py-mode lsp-pyright lsp-python-ms lsp-mode markdown-mode nose pip-requirements pipenv load-env-vars pippel poetry transient py-isort pydoc pyenv-mode pythonic pylookup pytest pyvenv sphinx-doc stickyfunc-enhance xcscope yapfify flycheck-elsa flycheck-package package-lint flycheck ws-butler writeroom-mode winum which-key volatile-highlights vim-powerline vi-tilde-fringe uuidgen use-package undo-tree treemacs-projectile treemacs-persp treemacs-icons-dired toc-org term-cursor symon symbol-overlay string-inflection string-edit spacemacs-whitespace-cleanup spacemacs-purpose-popwin spaceline-all-the-icons space-doc restart-emacs request rainbow-delimiters quickrun popwin pcre2el password-generator paradox overseer org-superstar open-junk-file nameless multi-line macrostep lorem-ipsum link-hint inspector info+ indent-guide hybrid-mode hungry-delete holy-mode hl-todo highlight-parentheses highlight-numbers highlight-indentation hide-comnt help-fns+ helm-xref helm-themes helm-swoop helm-purpose helm-projectile helm-org helm-mode-manager helm-make helm-descbinds helm-ag google-translate golden-ratio font-lock+ flx-ido fancy-battery eyebrowse expand-region evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-textobj-line evil-surround evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state evil-lion evil-indent-plus evil-iedit-state evil-goggles evil-exchange evil-evilified-state evil-escape evil-ediff evil-collection evil-cleverparens evil-args evil-anzu eval-sexp-fu emr elisp-slime-nav elisp-def editorconfig dumb-jump drag-stuff dotenv-mode dired-quick-sort diminish devdocs define-word column-enforce-mode clean-aindent-mode centered-cursor-mode auto-highlight-symbol auto-compile aggressive-indent ace-link ace-jump-helm-line))
   '(safe-local-variable-values '((eval progn (pp-buffer) (indent-buffer))))
   '(warning-suppress-types '((org-element-cache) (frameset) (frameset) (frameset))))
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   )
  )

                                        ; LocalWords:  arbeitsblockerics-importiert
