# .bashrc

# Check if the shell is interactive
[[ $- != *i* ]] && return

# Enable color
use_color=true

# Check window size
shopt -s checkwinsize

# Enable alias expansion
shopt -s expand_aliases

# Enable automatic directory changing
shopt -s autocd

# Append to history
shopt -s histappend

# Ignore leading spaces in history
HISTCONTROL=ignorespace

# Source bash aliases if available
[ -r /home/$USER/.bash_aliases ] && . /home/$USER/.bash_aliases

# Source local configuration if available
[ -r /home/$USER/.localrc ] && . /home/$USER/.localrc

# Source color settings for tty if available
[ -r /home/$USER/.cache/wal/colors-tty.sh ] && source /home/$USER/.cache/wal/colors-tty.sh

# Additional configurations or aliases can be added here

