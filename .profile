# .profile

PATH=$PATH:$HOME/.local/bin/

export EDITOR="nvim"
export BROWSER="firefox"
export MAIL="thunderbird"
