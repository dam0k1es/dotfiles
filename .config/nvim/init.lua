-- Plugins :

-- Lazy package manager setup
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath })
end
vim.opt.rtp:prepend(vim.env.LAZY or lazypath)

-- Package setup
require("lazy").setup({
    -- Theme
    {
        "folke/tokyonight.nvim",
        lazy = false,
        priority = 1000,
        opts = {
            transparent = true,
            styles = {
                sidebars = "transparent",
                floats = "transparent",
            },
        },
    },
    {
        'goolord/alpha-nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        config = function ()
            require'alpha'.setup(require'alpha.themes.startify'.config)
        end,
        event = "VimEnter",
    },
    -- Commandline
    {
        'VonHeikemen/fine-cmdline.nvim',
        event = "VeryLazy",
    },
    -- Fuzzy Finder (Package: ripgrep)
    {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.6',
        dependencies = { 'nvim-lua/plenary.nvim' },
        event = "VeryLazy",
    },
    -- File Explorer
    {
        "nvim-neo-tree/neo-tree.nvim",
        branch = "v3.x",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons", 
            "MunifTanjim/nui.nvim",
        },
        event = "VeryLazy"
    },
    -- Statusline
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        opts = function()
            return {
                options = {
                    theme = "horizon"
                }
            }
        end,
        event = "VimEnter",
    },
    -- Bookmarks
    {
        'tomasky/bookmarks.nvim',
        event = "VimEnter",
        config = function()
            require('bookmarks').setup()
        end,
        event = "VeryLazy",
    },
    {
        "folke/flash.nvim",
        event = "VeryLazy"
    },
    -- Commenting
    {
        'numToStr/Comment.nvim',
        opts = {},
        event = "VimEnter",
    },
    -- Help with keycombinations
    {
        "folke/which-key.nvim",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        event = "VimEnter",
    },
	{ 'echasnovski/mini.nvim', version = false },
    -- Improved yanking
    {
        "gbprod/yanky.nvim",
        opts = {},
        event = "VeryLazy",
    },
    -- LSP
    {
        "neovim/nvim-lspconfig",
        event = "VimEnter",
    },
    -- Syntax Highlighting  (Package: tree-sitter)
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function () 
            local configs = require("nvim-treesitter.configs")
            configs.setup({
                auto_install = true,
                sync_install = false,
                highlight = { enable = true },
                indent = { enable = true },  
            })
        end,
        event = "VimEnter",
    },
    -- Autocompletion
    {
        "hrsh7th/nvim-cmp",
        version = false,
        dependencies = {
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-nvim-lua",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-cmdline",
            "chrisgrieser/cmp_yanky",
            "saadparwaiz1/cmp_luasnip",
        },
        opts = function(_, opts)
            vim.api.nvim_set_hl(0, "CmpGhostText", { link = "Comment", default = true })
            local cmp = require("cmp")
            return {
                completion = {
                    completeopt = "menu,menuone,noinsert",
                },
                sources = cmp.config.sources({
                    { name = "nvim_lsp" },
                    { name = "nvim_lua" },
                    { name = "path" },
                    { name = "buffer" },
                    { name = "cody" },
                    { name = "luasnip" },
                }),
                mapping = cmp.mapping.preset.insert({
                    ['<CR>'] = cmp.mapping.confirm({select = true}),
                    ['<Tab>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
                    ['<S-Tab>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
                    ['<C-Space>'] = cmp.mapping.complete(),
                })
            }
        end,
        event = "InsertEnter",
    },
    -- Snippets
    {
        "L3MON4D3/LuaSnip",
        version = "v2.*",
        build = "make install_jsregexp",
        dependencies = {
            {
                "rafamadriz/friendly-snippets",
                config = function() 
                    require("luasnip.loaders.from_vscode").lazy_load()
                end,
            },
            {
                "nvim-cmp",
                dependencies = {
                    "saadparwaiz1/cmp_luasnip",
                },
                opts = function(_, opts)
                    opts.snippet = {
                        expand = function(args)
                            require("luasnip").lsp_expand(args.body)
                        end,
                    }
                    table.insert(opts.sources, { name = "luasnip" })
                end,
            },
        },
        event = "VeryLazy",
    },
    -- Convert yasnippets to vscode_luasnippets
    {
        "smjonas/snippet-converter.nvim",
        config = function()
            local macstovim = {
                sources = {
                    yasnippet = {
                        "/home/$USER/.emacs.d/private/snippets/"
                    },
                },
                output = {
                    vscode_luasnip = {
                        vim.fn.stdpath("config") .. "/snippets",
                    },
                },
            }
            local vimtomacs = {
                sources = {
                    vscode_luasnip = {
                        vim.fn.stdpath("config") .. "/snippets",
                    },
                },
                output = {
                    yasnippet = {
                        "/home/$USER/.emacs.d/private/snippets/"
                    }
                }
            }
            require("snippet_converter").setup {
                templates = { 
                    vimtomacs, 
                    macstovim 
                },
            }
        end,
        event = "VeryLazy"
    },
    -- Embedded Development
    {
        "anurag3301/nvim-platformio.lua",
        dependencies = {
            { "akinsho/nvim-toggleterm.lua" },
            { "nvim-telescope/telescope.nvim" },
            { "nvim-lua/plenary.nvim" },
        },
        event = "VeryLazy"
    },
    -- LLama Support
    {
        "nomnivore/ollama.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
        opts = {
            model = "magicoder",
        },
    },
    -- Code Completion
    -- {
    --     "sourcegraph/sg.nvim",
    --     dependencies = { "nvim-lua/plenary.nvim", --[[ "nvim-telescope/telescope.nvim ]] },
    --
    --     -- If you have a recent version of lazy.nvim, you don't need to add this!
    --     build = "nvim -l build/init.lua",
    -- },
})

-- Plugin setup

vim.cmd("colorscheme tokyonight-storm")
 
require('lualine').setup()

local ls = require("luasnip")
ls.config.set_config({
    history = true,
    enable_autosnippets = true,})
require("luasnip/loaders/from_vscode").lazy_load({
    paths = {
        vim.fn.stdpath('config') .. './',
    }
})

require("lspconfig").bashls.setup{}
require("lspconfig").pyright.setup{}
require("lspconfig").clangd.setup{}

require('lspconfig').nil_ls.setup {
    autostart = true,
    settings = {
        ['nil'] = {
            formatting = {
                command = { "nixpkgs-fmt" },
            },
        },
    }
}

require('lspconfig').ltex.setup{
    on_attach = on_attach,
    cmd = { "ltex-ls" },
    flags = { debounce_text_changes = 300 },
    settings = {
        ltex = {
            language = "de-DE",
            additionalRules = {
                enablePickyRules = true,
                motherTongue = "de-DE",
            },
        }
    }
}

-- local function enable_cody()
--
--     -- Liste der unterstützten Dateierweiterungen
--     local cody_filetypes = {
--         "c",
--         "cpp",
--         "java",
--         "python",
--         "lua",
--     }
--     -- Hole die aktuelle Dateierweiterung
--     local fileExtension = vim.fn.expand("%:e")
--
--     -- Überprüfe, ob die Dateierweiterung in der Liste der unterstützten Dateitypen enthalten ist
--     for _, ext in ipairs(cody_filetypes) do
--         if fileExtension == ext then
--             require("sg").setup {
--                 --        - gd -> goto definition
--                 --        - gr -> goto references
--             }
--         end
--     end
-- end
--
-- enable_cody()

-- Keymaps --

local ts =  require('telescope.builtin')
local tse = require('telescope').extensions
local bookmarks = require("bookmarks")
local flash = require("flash")
local ls = require("luasnip")

vim.keymap.set("i", "<tab>", "<cmd>lua require'luasnip'.jump(1)<CR>", opts)
vim.keymap.set("s", "<tab>", "<cmd>lua require'luasnip'.jump(1)<CR>", opts)
vim.keymap.set("i", "<S-tab>", "<cmd>lua require'luasnip'.jump(-1)<CR>", opts)
vim.keymap.set("s", "<S-tab>", "<cmd>lua require'luasnip'.jump(-1)<CR>", opts)

vim.g.mapleader = " "

local wk = require("which-key")

wk.register({
    ["<A-y>"] = { function() tse.yank_history.yank_history({}) end, "Yank history" },
    ["p"] = { "<Plug>(YankyPutAfter)", "Paste after" },
    ["P"] = { "<Plug>(YankyPutBefore)", "Paste before" },
    ["gp"] = { "<Plug>(YankyGPutAfter)", "Global paste after" },
    ["gP"] = { "<Plug>(YankyGPutBefore)", "Global paste before" },
    ["<c-p>"] = { "<Plug>(YankyPreviousEntry)", "Previous yank entry" },
    ["<c-n>"] = { "<Plug>(YankyNextEntry)", "Next yank entry" },
    ["<leader><leader>"] = { "<cmd>FineCmdline<CR>", "Open FineCmdline" },
    ["<leader>a"] = { "<cmd><CR>", "AI" },
    ["<leader>al"] = { ':lua require("ollama").prompt()<CR>', "Ollama Prompt" },
    ["<leader>ac"] = { ':lua require("sg").setup()<CR>', "Cody completion" },
    ["<leader>b"] = { "<cmd><CR>", "Buffers" },
    ["<leader>bb"] = { ts.buffers, "Show buffers" },
    ["<leader>f"] = { "<cmd><CR>", "Files"},
    ["<leader>ff"] = { ts.find_files, "Find files" },
    ["<leader>ft"] = { '<cmd>Neotree toggle left<CR>', "Show filemanager" },
    ["<leader>fr"] = { ts.oldfiles, "Show old files" },
    ["<leader>h"] = { "<cmd><CR>", "Help" },
    ["<leader>hh"] = { ts.help_tags, "Show help tags" },
    ["<leader>j"] = { "<cmd><CR>", "Jump" },
    ["<leader>jj"] = { function() flash.remote() end, "Flash remote" },
    ["<leader>jm"] = { function() flash.treesitter() end, "Flash treesitter" },
    ["<leader>js"] = { function() flash.treesitter_search() end, "Flash treesitter search" },
    ["<leader>jc"] = { "`.", "Jump to last change" },
    ["<leader>m"] = { "<cmd><CR>", "Bookmarks" },
    ["<leader>mm"] = { bookmarks.bookmark_toggle, "Toggle bookmark" },
    ["<leader>mc"] = { bookmarks.bookmark_clean, "Clean bookmarks" },
    ["<leader>mn"] = { bookmarks.bookmark_next, "Next bookmark" },
    ["<leader>mp"] = { bookmarks.bookmark_prev, "Previous bookmark" },
    ["<leader>ml"] = { function() tse.bookmarks.list() end, "List bookmarks" },
    ["<leader>s"] = { "<cmd><CR>", "Search" },
    ["<leader>ss"] = { ts.current_buffer_fuzzy_find, "Fuzzy find in current buffer" },
    ["<leader>sg"] = { ts.live_grep, "Live grep" },
    ["<leader>S"] = { "<cmd><CR>", "Spelling" },
    ["<leader>SS"] = { ts.spell_suggest, "Spell suggest" },
    ["<leader>S"] = { "<cmd><CR>", "Toggle" },
    ["<leader>tl"] = { ":set nonu nornu!<CR>", "Toggle line numbers" },
    ["<leader>th"] = { ":set hlsearch! hlsearch?<CR>", "Toggle search highlights" },
    ["<leader>c"] = { "<cmd><CR>", "Compile" },
    ["<leader>cg"] = { ":w<CR> :!gcc % -o %< && ./%<CR>", "Compile and execute file (C)" },
    ["<leader>cl"] = { ":w<CR> :!latexmk -f -interaction=nonstopmode %<CR>", "Compile and execute file (LaTeX)" },
    ["<leader>v"] = { "<cmd><CR>", "View" },
    ["<leader>vl"] = { ":w<CR> :!xdg-open %:r.pdf<CR><CR>", "Open generated PDF" },
})


-- COMMANDS
-- :W sudo saves the file
vim.cmd('command! W w !sudo tee % > /dev/null')

-- Options --

-- linenumbers
vim.opt.number = true
vim.opt.numberwidth = 5
vim.opt.relativenumber = true
vim.opt.cursorline = true

-- search settings
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = true

-- tabs
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.autoindent = true

-- Enable mouse support, unless in insert mode
vim.opt.mouse = "nvch"

-- Set to auto read when a file is changed from the outside
vim.opt.autoread = true

-- Persistent Historys
local undo_dir = vim.fn.expand(vim.fn.stdpath('data') .. '/undodir')
vim.opt.undofile = true
vim.opt.undodir = undo_dir 
if vim.fn.isdirectory(undo_dir) == 0 then
    vim.fn.mkdir(undo_dir, 'p')
end

-- Automatic Backup
local backup_dir = vim.fn.expand(vim.fn.stdpath('data') .. '/backup')
vim.opt.backup = true
vim.o.backupdir = backup_dir
if vim.fn.isdirectory(backup_dir) == 0 then
    vim.fn.mkdir(backup_dir, 'p')
end
