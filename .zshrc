bindkey -s "^b" 'tmux^M'
bindkey -s "^w" 'lynx^M'

export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="jonathan"

plugins=(
	z
	fzf
	git
    tmux
	sudo
	# bgnotify
	dirhistory
	command-not-found
	colored-man-pages
	zsh-syntax-highlighting
    zsh-autocomplete
	zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh

bindkey '^R' fzf-history-widget
bindkey '\e[A' fzf-history-widget
bindkey '\eOA' fzf-history-widget
bindkey '^[[A' fzf-history-widget

# Show ls after cd (zsh hook)
chpwd() {
if [ $(ls -A | wc -w) -le 100 ] ;
then
  if [ $(ls -A | wc -w) -le 25 ] ;
  then
    lsd -A --hyperlink=auto
  else
    lsd --hyperlink=auto
  fi
fi
}

HISTSIZE=10000000
SAVEHIST=10000000
setopt autocd # change directory just by typing its name
setopt interactivecomments # allow comments in interactive mode
setopt magicequalsubst     # enable filename expansion for arguments of the form ‘anything=expression’
setopt nonomatch           # hide error message if there is no match for the pattern
setopt notify              # report the status of background jobs immediately
setopt numericglobsort     # sort filenames numerically when it makes sense
setopt promptsubst         # enable command substitution in prompt
setopt correct             # spelling correction for commands
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt HIST_IGNORE_SPACE   # ignore commands prefixed with a space
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.

# Environment variables
[ -r /home/$USER/.profile ] && . /home/$USER/.profile

# Aliases
[ -r /home/$USER/.bash_aliases ] && . /home/$USER/.bash_aliases

# Local Configurations
[ -r /home/$USER/.localrc ] && . /home/$USER/.localrc

# Autostart tmux
if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
    if [[ $(tmux list-sessions | grep main) = '' ]]; then
        tmux new -s main 
    else 
        tmux attach -t main
    fi
fi
