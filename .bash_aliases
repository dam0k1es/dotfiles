#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    	 #administrate#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

alias hswitch='home-manager switch -b backup$RANDOM --flake ~/Dokumente/Prog/git/dotfiles/home-manager/  ; hyprctl reload'
alias nswitch='sudo nixos-rebuild switch --flake ~/Dokumente/Prog/git/dotfiles/nixos/#'
alias update='sudo apt update ; sudo apt upgrade -y ; sudo apt autoremove -y ; sudo apt clean ; sudo snap refresh ; flatpak update ; nix-env -u'
alias :q='exit'                                             

alias sys='sudo systemctl'

alias se='sudo $EDITOR'

wsudo() {
xhost +SI:localuser:root
sudo "$@"
xhost -SI:localuser:root
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
    	    #navigate#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

cb() {
    cd "$(for i in {1.."$1"}; do echo -n '../'; done)"
}

cl() {
	readlink -f "$@" | sed 's/^/"/;s/$/"/' | wl-copy
}

alias workd='mkdir /tmp/WorkD 2>/dev/null ; cd /tmp/WorkD'

alias shred='shred -u'

alias texclean='rm *.{aux,bbl,bcf,blg,log,nav,out,run.xml,snm,synctex.gz,toc}'

alias loc='locate -A -i'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
	         #EDIT#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#
ec(){
[ "$(pgrep emacs)" == "" ] && emacs --daemon=Default
nohup emacsclient --socket=Default -c $@ & 
}
et(){
[ "$(pgrep emacs)" == "" ] && emacs --daemon=Default
emacsclient --socket=Default -c -nw $@
}

alias aliases='$EDITOR ~/.bash_aliases && . ~/.bash_aliases'

alias clipboard=' xclip -selection clipboard'

page() { "$@" | less ; }

alias ccat='pygmentize -g'

cless() { pygmentize -g $1 | less ; }

cpage() { "$@" > /tmp/cpage && pygmentize -g /tmp/cpage | less ; }

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
	          #FILES#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

ex ()                                                       # Unarchive (Manjaro)
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf "$1"   ;;
      *.tar.gz)    tar xzf "$1"   ;;
      *.bz2)       bunzip2 "$1"   ;;
      *.rar)       unrar x "$1"     ;;
      *.gz)        gunzip  "$1"    ;;
      *.tar)       tar xf  "$1"    ;;
      *.tbz2)      tar xjf "$1"   ;;
      *.tgz)       tar xzf "$1"   ;;
      *.zip)       unzip   "$1"     ;;
      *.Z)         uncompress "$1";;
      *.7z)        7z x "$1"      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
# Go Back n times
    echo "'$1' is not a valid file"
  fi
}

alias _='xdg-open'

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
             #FORMAT#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

alias df='df -h'                                            # human-readable sizes
alias du='du -h'
alias free='free -m'

alias shellcheck='shellcheck --color=always -x '

alias l='lsd --hyperlink=auto'
alias ll='lsd -l --hyperlink=auto'
alias la='lsd -lA --hyperlink=auto'
alias tree='lsd --tree --hyperlink=auto'

function mdcat()                                            # Format output
{
    markdown < "$@" | html2text
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
             #OTHER#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

alias llm='ollama run magicoder'
unllm(){
    DATA='{"model": "'$1'", "keep_alive": 0}'
    curl http://localhost:11434/api/generate -d "$DATA"
}
rellm(){
	sudo systemctl stop ollama
        sudo rmmod nvidia_uvm && sudo modprobe nvidia_uvm
	sudo systemctl start ollama
}
wllm(){
    rg $(ps aux | grep -- '--model' | grep -v grep | grep -Po '(?<=--model\s).*' | cut -d ' ' -f1 | cut -d '/' -f 6 | cut -d '-' -f2) /mnt/data/ollama | rg library | cut -d '/' -f8
}
